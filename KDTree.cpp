#include "KDTree.h"

template <class T>
struct Vec2DComp {
	Vec2DComp(int _i) { i = _i; };
	bool operator() (vector<T> A, vector<T> B) { return (A[i] < B[i]); }
	int i;
};

template <class T>
KDTree<T>::KDTree(const int& dim) {

	dimension = dim;
}

template <class T>
void KDTree<T>::BuildTree(vector2D<T> data) {


	root = BuildTreeHelper(data, 0, data.size(), 0);

}

template <class T>
Node<T>* KDTree<T>::BuildTreeHelper(vector2D<T>& data, const int& l, const int& r, int dim) {

	Node<T>* node = new Node<T>();

	int median_index = (l + r) / 2;
	//node->data = quick_select(data, median_index, l, r, dim); // returns the median vector of dimension dim & 2d vector is 3 way partitioned into <, ==, and >

	Vec2DComp<T> compareObject = Vec2DComp<T>(dim);
	sort(data.begin() + l, data.begin() + r, compareObject);


	/*
	cout << "l: " << l << endl;
	cout << "r: "<< r << endl;


	cout << "++++++" << endl;
	for (int i = l; i < r; ++i) {
		for (int j = 0; j < data[i].size(); ++j) {

			cout << data[i][j] << " ";
		}
		cout << endl;
	}
	cout << "++++++" << endl;
	*/



	int num = (r - l);
	if (num == 1){

		node->data = data[l];
		return node;
	}
	else if (num == 2) {

		node->data = data[r - 1];
		Node<T>* children = new Node<T>();
		children->data = data[l];
		node->left = children;

		return node;
	}

	node->data = data[median_index];

	/*
	cout << "median: " << endl;
	for (int i = 0; i < node->data.size(); ++i) {

		cout << node->data[i] << " ";
	}
	cout << "@@@@" << endl;
	*/



	dim = (dim + 1) % dimension;

	node->left = BuildTreeHelper(data, l, median_index, dim);
	node->right = BuildTreeHelper(data, median_index + 1, r, dim);


	return node;
}


template <class T>
vector<T> KDTree<T>::NearestNeighborSearch(const vector<T>& val) {

	//out<<"NearestNeighborSearch"<<endl;

	assert(root != nullptr);
	assert(val.size() == root.data.size());

	double best = numeric_limits<double>::max();

	Node<T>* result = NearestNeighborSearchHelper(root, val, 0, best);

	return result->data;
}


template <class T>
Node<T>* KDTree<T>::NearestNeighborSearchHelper(Node<T>* node, const vector<T>& val, int dim, double& best) {

	if (!node) { // leaf node
		return nullptr;
	}

	Node<T>* nearest;
	Node<T>* next;

	/*
	cout << "1: " << endl;
	for (int i = 0; i < node->data.size(); ++i) {

		cout << node->data[i] << " ";
	}
	cout << "@@@@" << endl;
	*/



	if (val[dim] <= node->data[dim]) {

		next = node->left;

		nearest = NearestNeighborSearchHelper(next, val, (dim + 1) % node->data.size(), best);
	}
	else { //val.data[dim] <= node->data[dim]

		next = node->right;

		nearest = NearestNeighborSearchHelper(next, val, (dim + 1) % node->data.size(), best);
	}


	// check for closer neighbor
	double distance = EuclideanDistance(node->data, val);

	if (distance < best) {

		nearest = node;
		best = distance;
	}

	// check if nodes in other splitting plane are closer
	if (abs(val[dim] - node->data[dim]) < best) {

		//cout << "Check other side" << endl;


		if (next == node->right) {
			next = node->left;
		}
		else {
			next = node->right;
		}

		double newbest = best;
		Node<T>* newnearest = NearestNeighborSearchHelper(next, val, (dim + 1) % node->data.size(), newbest);

		if (newbest < best) {
			best = newbest;
			nearest = newnearest;
		}
	}

	return nearest;

}


template <class T>
Node<T>* KDTree<T>::Root() {

	return root;
}


template <class T>
void InOrderTraversal(Node<T>* root){

	Node<T>* cursor = root;

	if (cursor != nullptr){

		InOrderTraversal(cursor->left);

		cout << "-----" << endl;
		for (int i = 0; i < cursor->data.size(); ++i) 
			cout << cursor->data[i] << " ";
		cout << "-----" << endl;

		InOrderTraversal(cursor->right);
	}
}

template <class T>
double EuclideanDistance(const vector<T>& A, const vector<T>& B) {

	// do not square root

	double sum = 0;
	for (int i = 0; i < A.size(); ++i) {

		sum += pow(A[i] - B[i], 2);
	}

	return sum;
}

/*
template <class T>
vector<T> quick_select(vector2D<T>& data, const int& k, const int& l, const int& r, const int& dim) {

	// l <= r
	// k: index

	if ((r - l) == 1)
		return data[l];

	// select random pivot
	int pivot_index = (rand() % (r - l)) + l;
	T pivot = data[pivot_index][dim];

	ThreeWayPartition(data, pivot, l, r, dim);

	// get num of lows and num of equals
	int low_num = 0;
	int i = l;

	while (i < r && data[i][dim] < pivot) {

		++i;
		++low_num;
	}

	int equal_num = 0;

	while (i < r && data[i][dim] < pivot) {

		++i;
		++equal_num;
	}


	if (k < low_num) {

		return quick_select(data, k, l, low_num, dim);
	}
	else if (k < low_num + equal_num) {

		return data[pivot_index];

	}
	else {
		// k > low_num + equal_num

		return quick_select(data, k, low_num + equal_num, r, dim);
	}


}

template <class T>
void ThreeWayPartition(vector2D<T>& data, const T& pivot, const int& l, const int& r, const int& dim) {


}
*/