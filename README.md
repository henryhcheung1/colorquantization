This repository will contain color quantization algorithms that reduce the number of distinct colors in an image. 
Quantization is a lossy compression technique that compresses a range of values to a single value. Applications include 
displaying images on devices that can only display a limited range of colors, due to memory limitations. 

Algorithms

Nearest Neighbor K-Dimensional Tree

WIP