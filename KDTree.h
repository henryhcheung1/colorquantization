#pragma once

#include <iostream>
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <stdio.h> 
#include <algorithm>
#include <limits>
#include <assert.h>

using namespace std;

template <class T>
struct Node {

	vector<T> data;
	Node<T>* left = nullptr;
	Node<T>* right = nullptr;

};


template <class T>
using vector2D = vector<vector<T>>;


template <class T>
class KDTree
{
public:

	KDTree(const int& dim);
	//~KDTree();

	void BuildTree(vector2D<T> data);

	vector<T> NearestNeighborSearch(const vector<T>& val);


	Node<T>* Root();


private:

	Node<T>* BuildTreeHelper(vector2D<T>& data, const int& l, const int& r, int dim);
	Node<T>* NearestNeighborSearchHelper(Node<T>* node, const vector<T>& val, int dim, double& best);

	int dimension;
	Node<T>* root = nullptr;

};

template <class T>
void InOrderTraversal(Node<T>* cursor);

/*
template <class T>
vector<T> quick_select(vector2D<T>& data, const int& k, const int& l, const int& r, int& dim);

template <class T>
void ThreeWayPartition(vector2D<T>& data, const T& pivot, const int& l, const int& r, const int& dim);
*/

template <class T>
double EuclideanDistance(const vector<T>& A, const vector<T>& B);