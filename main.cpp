#include <Magick++.h> 
#include "KDTree.h"
#include "KDtree.cpp"

using namespace Magick;

vector<int> HexToRGB(int hex);
vector2D<int> ReadHexToRGB(vector<int> data);

const int DIMENSIONS = 3;

int main(int argc, char** argv)
{

	/*

	// kdtree test

	KDTree<int> A = KDTree<int>(3);

	vector2D<int> data;
	vector<int> a{ 5, 4 };
	vector<int> b{ 2, 3 };
	vector<int> c{ 8, 1 };
	vector<int> d{ 9, 6 };
	vector<int> e{ 7, 2 };
	vector<int> f{ 4, 9 };


	data.push_back(a);
	data.push_back(b);
	data.push_back(c);
	data.push_back(d);
	data.push_back(e);
	data.push_back(f);

	A.BuildTree(data);

	InOrderTraversal(A.Root());


	vector<int> g{ 8, 1 };


	Node<int>* x = A.NearestNeighborSearch(g);

	for (int i = 0; i < x->data.size(); ++i) {
		cout << x->data[i] << " ";
	}
	cout << "end" << endl;

	*/



	srand((unsigned)time(NULL)); 

	// CSS2 colors
	//vector<int> hexcolors{ 0x000000, 0xc0c0c0, 0x808080, 0xffffff, 0x800000, 0xff0000, 0x800080, 0xff00ff, 0x008000,
	//	0x00ff00, 0x808000, 0xffff00, 0x000080, 0x0000ff, 0x008080, 0x00ffff, 0xffa500 };


	// CSS4
	vector<int> hexcolors{ 0x000000, 0xc0c0c0, 0x808080, 0xffffff, 0x800000, 0xff0000, 0x800080, 0xff00ff, 0x008000,
		0x00ff00, 0x808000, 0xffff00, 0x000080, 0x0000ff, 0x008080, 0x00ffff, 0xffa500, 0xf0f8ff, 0xfaebd7, 0x7fffd4,
		0xf0ffff, 0xf5f5dc, 0xffe4c4, 0xffebcd, 0x8a2be2, 0xa52a2a, 0xdeb887, 0x5f9ea0, 0x7fff00, 0xd2691e, 0xff7f50,
		0x6495ed, 0xfff8dc, 0xdc143c, 0x00ffff, 0x00008b, 0x008b8b, 0xb8860b, 0xa9a9a9, 0x006400, 0xa9a9a9, 0xbdb76b,
		0x8b008b, 0x556b2f, 0xff8c00, 0x9932cc, 0x8b0000, 0xe9967a, 0x8fbc8f, 0x483d8b, 0x2f4f4f, 0x2f4f4f, 0x00ced1,
		0x9400d3, 0xff1493, 0x00bfff, 0x696969, 0x696969, 0x1e90ff, 0xb22222, 0xfffaf0, 0x228b22, 0xdcdcdc, 0xf8f8ff,
		0xffd700, 0xdaa520, 0xadff2f, 0x808080, 0xf0fff0, 0xff69b4, 0xcd5c5c, 0x4b0082, 0xfffff0, 0xf0e68c, 0xe6e6fa,
		0xfff0f5, 0x7cfc00, 0xfffacd, 0xadd8e6, 0xf08080, 0xe0ffff, 0xfafad2, 0xd3d3d3, 0x90ee90, 0xd3d3d3, 0xffb6c1,
		0xffa07a, 0x20b2aa, 0x87cefa, 0x778899, 0x778899, 0xb0c4de, 0xffffe0, 0x32cd32, 0xfaf0e6, 0xff00ff, 0x66cdaa,
		0x0000cd, 0xba55d3, 0x9370db, 0x3cb371, 0x7b68ee, 0x00fa9a, 0x48d1cc, 0xc71585, 0x191970, 0xf5fffa, 0xffe4e1,
		0xffe4b5, 0xffdead, 0xfdf5e6, 0x6b8e23, 0xff4500, 0xda70d6, 0xeee8aa, 0x98fb98, 0xafeeee, 0xdb7093, 0xffefd5,
		0xffdab9, 0xcd853f, 0xffc0cb, 0xdda0dd, 0xb0e0e6, 0xbc8f8f, 0x4169e1, 0x8b4513, 0xfa8072, 0xf4a460, 0x2e8b57,
		0xfff5ee, 0xa0522d, 0x87ceeb, 0x6a5acd, 0x708090, 0x708090, 0xfffafa, 0x00ff7f, 0x4682b4, 0xd2b48c, 0xd8bfd8,
		0xff6347, 0x40e0d0, 0xee82ee, 0xf5deb3, 0xf5f5f5, 0x9acd32, 0x663399 };


	vector2D<int> rgbdata = ReadHexToRGB(hexcolors);

	// build tree

	KDTree<int> tree = KDTree<int>(DIMENSIONS);

	tree.BuildTree(rgbdata);


	//InOrderTraversal(tree.Root());


	

	//InitializeMagick("C:\Program Files\ImageMagick");

	// read image

	Image image;

	image.read("Images\\bridge.jpg");
	image.type(TrueColorType);
	image.modifyImage();

	size_t rows = image.rows();
	size_t columns = image.columns();

	cout<<"rows: "<< rows <<endl;
	cout << "columns: " << columns << endl;

	Pixels view(image);
	Quantum* pixels = view.get(0, 0, columns, rows); // get image into cache

	for (unsigned int j = 0; j < rows; j++) {

		//Quantum* pixels = view.get(0, j, columns, 1); // get pixel into cache

		for (unsigned int i = 0; i < columns; i++) {

			Quantum* temppixel = pixels;

			//int r = 255 * QuantumScale * *pixels++;
			//int g = 255 * QuantumScale * *pixels++;
			//int b = 255 * QuantumScale * *pixels++;

			//cout << "r, g, b: " << r << ", " << g << ", " << b << endl;

			int r = *pixels++;
			int g = *pixels++;
			int b = *pixels++;

			vector<int> pixel{r, g, b};

			vector<int> neighbor = tree.NearestNeighborSearch(pixel);

			for (int i = 0; i < neighbor.size(); ++i) {

				*temppixel++ = neighbor[i];
			}

			/*
			cout << "nearest neighnor" << endl;
			for (int i = 0; i < neighbor.size(); ++i) {

				cout << neighbor[i] << " ";
			}
			cout << endl << endl;
			*/


		}
	}

	// nn_color = input rgb into kd tree

	// replace rgb with nn_color

	




	view.sync();

	image.write("Images\\bridgeCSS4.jpg");

	

	return 0;
}

vector<int> HexToRGB(int hex) {

	// 16 bits per color channel since imagemagick is 16 bits;

	vector<int> rgb;

	rgb.push_back((((hex >> 16) & 0xFF) / 255.0) * QuantumRange);
	rgb.push_back((((hex >> 8) & 0xFF) / 255.0) * QuantumRange);
	rgb.push_back(((hex & 0xFF) / 255.0) * QuantumRange);

	/*
	for (int i = 0; i < rgb.size(); ++i) {

		cout << rgb[i] << " ";
	}
	cout << endl;
	*/

	return rgb;
}

vector2D<int> ReadHexToRGB(vector<int> data) {

	vector2D<int> output;

	for (int i = 0; i < data.size(); ++i) {

		output.push_back(HexToRGB(data[i]));
	}

	return output;
}